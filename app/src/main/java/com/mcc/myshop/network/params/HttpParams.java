package com.mcc.myshop.network.params;

/**
 * Created by Ashiq on 8/13/2016.
 */
public class HttpParams {

  public static final String BASE_URL = "https://mccltd.info/projects/myshop/wp-json/wc/v2/";

  public static final String CONSUMER_KEY = "ck_988a1ae15a28a3e7749c9d4337ae809795027250";
  public static final String CONSUMER_SECRET = "cs_1a88d8033d70dfea7d85cc23abcdc8014ed5df96";

  // END POINTS
  public static final String API_CATEGORIES = BASE_URL + "products/categories";
  public static final String API_PRODUCTS_LIST = BASE_URL + "products?page=";
  public static final String API_PRODUCTS_BY_CATEGORY = BASE_URL + "products?category=";
  public static final String API_PRODUCT_DETAILS = BASE_URL + "products/";
  public static final String API_COUPONS = BASE_URL + "coupons";
  public static final String API_PRODUCT_REVIEWS = "/reviews";

  // product filtered/search key constants
  public static final String KEY_PER_PAGE = "&per_page=";
  public static final String KEY_SEARCH = "&search=";
  public static final String KEY_FEATURED = "&featured=true";
  public static final String KEY_RECENT = "&after=";
  public static final String KEY_CATEGORY = "&category=";
  public static final String KEY_MIN_PRICE = "&min_price=";
  public static final String KEY_MAX_PRICE = "&max_price=";
  public static final String KEY_ORDER = "&order=";
  public static final String KEY_ORDER_BY = "&orderby=";

}
