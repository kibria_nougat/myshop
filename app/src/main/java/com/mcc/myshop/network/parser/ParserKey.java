package com.mcc.myshop.network.parser;

/**
 * Created by Nasir on 3/29/2017.
 */
public class ParserKey {

    // Common keys
    public static final String KEY_EMAIL = "email";

    // categories key
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_SHORT_DESC = "short_description";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_IMAGE_SOURCE = "src";

    // products and products details key
    public static final String KEY_IMAGES = "images";
    public static final String KEY_LINK_ADDRESS = "permalink";
    public static final String KEY_RELATED_IDS = "related_ids";
    public static final String KEY_CATEGORIES = "categories";
    public static final String KEY_PRICE = "price";
    public static final String KEY_SELL_PRICE = "sale_price";
    public static final String KEY_REGULAR_PRICE = "regular_price";
    public static final String KEY_TOTAL_SALE = "total_sales";
    public static final String KEY_PRICE_HTML = "price_html";
    public static final String KEY_CREATED_DATE = "date_created_gmt";
    public static final String KEY_MODIFIED_DATE = "date_modified_gmt";
    public static final String KEY_TYPE = "type";
    public static final String KEY_PUBLISH_STATUS = "publish";
    public static final String KEY_ON_SALE = "on_sale";
    public static final String KEY_IN_STOCK = "in_stock";
    public static final String KEY_REVIEW_ALLOW = "reviews_allowed";
    public static final String KEY_TOTAL_RATING = "rating_count";
    public static final String KEY_AVERAGE_RATING = "average_rating";
    public static final String KEY_ATTRIBUTE = "attributes";
    public static final String KEY_OPTIONS = "options";
    public static final String KEY_TRANSACTION_ID = "transaction_id";

    // Product reviews
    public static final String KEY_RATING = "rating";
    public static final String KEY_REVIEW = "review";

}
