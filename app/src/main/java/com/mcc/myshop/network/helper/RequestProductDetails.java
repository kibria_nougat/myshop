package com.mcc.myshop.network.helper;

import android.content.Context;
import android.util.Log;

import com.mcc.myshop.network.http.BaseHttp;
import com.mcc.myshop.network.http.ResponseListener;
import com.mcc.myshop.network.params.HttpParams;
import com.mcc.myshop.network.parser.ProductDetailParser;

import org.json.JSONObject;

/**
 * Created by Nasir on 5/11/17.
 */

public class RequestProductDetails extends BaseHttp {
    private Object object;
    private ResponseListener responseListener;

    public RequestProductDetails(Context context, String productId) {
        super(context, HttpParams.API_PRODUCT_DETAILS + productId);
    }

    public void setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
    }

    @Override
    public void onBackgroundTask(String response) {

        if (response != null && !response.isEmpty()){
            Log.e("Response", "Product details: " + response);
            try {
                object = new ProductDetailParser().getProductDetail(new JSONObject(response));
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskComplete() {
        if (responseListener != null) {
            responseListener.onResponse(object);
        }
    }
}
