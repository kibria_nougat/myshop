package com.mcc.myshop.network.http;


/**
 * Created by Ashiq on 8/13/2016.
 */
public interface ResponseListener {
     void onResponse(Object data);
}
