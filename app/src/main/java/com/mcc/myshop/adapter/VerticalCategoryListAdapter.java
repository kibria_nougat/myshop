package com.mcc.myshop.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mcc.myshop.R;
import com.mcc.myshop.listener.OnItemClickListener;
import com.mcc.myshop.model.Category;

import java.util.ArrayList;

public class VerticalCategoryListAdapter extends RecyclerView.Adapter<VerticalCategoryListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Category> dataList;

    // Listener
    public static OnItemClickListener mListener;

    public VerticalCategoryListAdapter(Context context, ArrayList<Category> dataList) {
        this.mContext = context;
        this.dataList = dataList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivCategory;
        private TextView tvCategoryName;

        public ViewHolder(final View itemView, int viewType) {
            super(itemView);

            ivCategory = (ImageView) itemView.findViewById(R.id.ivProductImage);
            tvCategoryName = (TextView) itemView.findViewById(R.id.tvCategoryName);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_vertical, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Category category = dataList.get(position);

        holder.tvCategoryName.setText(Html.fromHtml(category.name));

        Glide.with(mContext)
                .load(category.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.color.imgPlaceholder)
                .centerCrop()
                .into(holder.ivCategory);

        // listener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mListener.onItemListener(view, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public void setItemClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }
}
