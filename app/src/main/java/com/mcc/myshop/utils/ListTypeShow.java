package com.mcc.myshop.utils;

/**
 * Created by Nasir on 6/21/17.
 */

public enum ListTypeShow {
    LINEAR,
    GRID
}
