package com.mcc.myshop.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.mcc.myshop.R;
import com.mcc.myshop.adapter.AttributeAdapter;
import com.mcc.myshop.adapter.ProductSliderAdapter;
import com.mcc.myshop.adapter.ReviewListAdapter;
import com.mcc.myshop.data.constant.AppConstants;
import com.mcc.myshop.data.sqlite.FavouriteDBController;
import com.mcc.myshop.listener.OnItemClickListener;
import com.mcc.myshop.model.ProductAttribute;
import com.mcc.myshop.model.ProductDetail;
import com.mcc.myshop.model.ProductReview;
import com.mcc.myshop.network.helper.RequestProductReviews;
import com.mcc.myshop.network.helper.RequestProductDetails;
import com.mcc.myshop.network.http.ResponseListener;
import com.mcc.myshop.utils.ActivityUtils;
import com.mcc.myshop.utils.AppUtility;
import com.mcc.myshop.utils.RVEmptyObserver;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Nasir on 5/17/17.
 */

public class ProductDetailsActivity extends BaseActivity {

    // init variables
    private Context mContext;
    private Activity mActivity;

    // recycler view
    private RecyclerView rvReviews, rvAttributeSize;
    private ArrayList<ProductAttribute> attributeList;
    private ArrayList<ProductReview> reviewList;
    private ReviewListAdapter reviewListAdapter;
    private AttributeAdapter attributeAdapter;
    private LinearLayoutManager reviewLytManager, sizeLytManager;

    // init view
    private TextView tvProductName, tvOrderCount,
            tvAverageRating, tvRatingCount, priceSeparator,
            emptyView, tvRegularPrice, tvSalesPrice, tvShortDescription, tvContactNumber;
    private ProductDetail product = null;
    private ImageButton searchButton;
    private MaterialFavoriteButton addFavouriteList;
    private RatingBar ratingBar;
    private WebView wvDescription;
    private FloatingActionButton fabCall, fabMessage, fabMessenger;

    private ViewPager mPager;
    private ProductSliderAdapter productSliderAdapter;

    private boolean addedToFavouriteList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initToolbar();
        loadData();
        initListener();
    }

    private void initVariable() {
        mContext = getApplicationContext();
        mActivity = ProductDetailsActivity.this;
        attributeList = new ArrayList<>();
        reviewList = new ArrayList<>();
    }

    private void initView() {
        setContentView(R.layout.activity_product_details);

        initToolbar();
        enableBackButton();
        initLoader();

        // Reference ui
        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvShortDescription = (TextView) findViewById(R.id.tvShortDescription);
        tvOrderCount = (TextView) findViewById(R.id.tvOrderCount);
        tvAverageRating = (TextView) findViewById(R.id.tvAverageRating);
        tvRatingCount = (TextView) findViewById(R.id.tvRatingCount);
        tvRegularPrice = (TextView) findViewById(R.id.tvRegularPrice);
        tvSalesPrice = (TextView) findViewById(R.id.tvSalesPrice);
        tvContactNumber = (TextView) findViewById(R.id.tv_contact_number);
        priceSeparator = (TextView) findViewById(R.id.priceSeparator);
        wvDescription = (WebView) findViewById(R.id.wvDescription);
        mPager = (ViewPager) findViewById(R.id.vpImageSlider);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        addFavouriteList = (MaterialFavoriteButton) findViewById(R.id.addFavouriteList);
        searchButton = (ImageButton) findViewById(R.id.searchButton);
        fabCall = (FloatingActionButton) findViewById(R.id.fab_call);
        fabMessage = (FloatingActionButton) findViewById(R.id.fab_message);
        fabMessenger = (FloatingActionButton) findViewById(R.id.fab_messenger);

        // products attribute
        rvAttributeSize = (RecyclerView) findViewById(R.id.rvProductAttribute);
        rvAttributeSize.setHasFixedSize(true);
        rvAttributeSize.setNestedScrollingEnabled(false);
        sizeLytManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        rvAttributeSize.setLayoutManager(sizeLytManager);
        attributeAdapter = new AttributeAdapter(mContext, attributeList);
        rvAttributeSize.setAdapter(attributeAdapter);

        // init review list
        rvReviews = (RecyclerView) findViewById(R.id.rvReviews);
        reviewLytManager = new LinearLayoutManager(mActivity);
        rvReviews.setLayoutManager(reviewLytManager);
        reviewListAdapter = new ReviewListAdapter(reviewList);
        emptyView = (TextView) findViewById(R.id.emptyView);
        reviewListAdapter.registerAdapterDataObserver(new RVEmptyObserver(rvReviews, emptyView));
        rvReviews.setAdapter(reviewListAdapter);

        wvDescription.getSettings();
        wvDescription.getSettings().setTextZoom(85);
        wvDescription.setBackgroundColor(Color.TRANSPARENT);
        wvDescription.setVerticalScrollBarEnabled(false);

        // set contact phone number
        tvContactNumber.setText(AppConstants.CALL_NUMBER);
    }


    private void loadData() {
        Intent intent = getIntent();
        String productId = intent.getStringExtra(AppConstants.PRODUCT_ID);

        if (productId != null) {
            // Load product details

            //
            updateFavouriteButton(productId);

            RequestProductDetails requestProduct = new RequestProductDetails(mActivity, productId);
            requestProduct.setResponseListener(new ResponseListener() {
                @Override
                public void onResponse(Object data) {
                    if (data != null) {
                        product = (ProductDetail) data;

                        // set image
                        loadProductPhoto(product.imageList);
                        // set other properties
                        tvProductName.setText(product.name);
                        tvShortDescription.setText(Html.fromHtml(Html.fromHtml(product.shortDescription).toString()));//Html.fromHtml(product.shortDescription));
                        tvOrderCount.setText(String.valueOf(product.totalSale) + getString(R.string.orders));
                        if (product.onSaleStatus) {
                            tvRegularPrice.setText(String.valueOf(product.regularPrice));
                            tvSalesPrice.setText(AppConstants.CURRENCY + product.sellPrice);
                            tvRegularPrice.setPaintFlags(tvRegularPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        } else {
                            priceSeparator.setVisibility(View.INVISIBLE);
                            tvRegularPrice.setVisibility(View.INVISIBLE);
                            tvSalesPrice.setText(AppConstants.CURRENCY + product.regularPrice);
                        }

                        wvDescription.loadData(product.description, "text/html; charset=utf-8", "UTF-8");
                        tvAverageRating.setText(String.valueOf(product.averageRating));
                        tvRatingCount.setText(getResources().getString(R.string.total_review) + String.valueOf(product.totalRating) + ")");
                        ratingBar.setRating(product.averageRating);

                        // load product attributes
                        if (product.attributes.size() > AppConstants.VALUE_ZERO) {
                            rvAttributeSize.setVisibility(View.VISIBLE);
                            attributeList.addAll(product.attributes);
                            attributeAdapter.notifyDataSetChanged();
                        } else {
                            rvAttributeSize.setVisibility(View.GONE);
                        }
                    }
                    hideLoader();

                }
            });
            requestProduct.execute();

            // Load product reviews data
            RequestProductReviews productReviews = new RequestProductReviews(mActivity, productId);
            productReviews.setResponseListener(new ResponseListener() {
                @Override
                public void onResponse(Object data) {
                    if (data != null) {
                        reviewList.addAll((ArrayList<ProductReview>) data);
                        if (reviewList.size() > AppConstants.VALUE_ZERO) {
                            reviewListAdapter.notifyDataSetChanged();
                        }
                    }
                }
            });
            productReviews.execute();
        }
    }

    private void initListener() {

        addFavouriteList.setOnFavoriteChangeListener(new MaterialFavoriteButton.OnFavoriteChangeListener() {
            @Override
            public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                toggleFavouriteList();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.getInstance().invokeSearchActivity(mActivity, AppConstants.EMPTY_STRING);
            }
        });

        fabCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtility.makePhoneCall(mActivity, AppConstants.CALL_NUMBER);

            }
        });

        fabMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtility.sendSMS(mActivity, AppConstants.CALL_NUMBER, AppConstants.CONTACT_TEXT);
            }
        });

        fabMessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtility.invokeMessengerBot(mActivity);
            }
        });

    }

    private void loadProductPhoto(final ArrayList<String> images) {

        if (images != null && !images.isEmpty()) {
            productSliderAdapter = new ProductSliderAdapter(mContext, images);
            mPager = (ViewPager) findViewById(R.id.vpImageSlider);
            mPager.setAdapter(productSliderAdapter);
            CircleIndicator indicator = (CircleIndicator) findViewById(R.id.sliderIndicator);
            indicator.setViewPager(mPager);

            productSliderAdapter.setItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemListener(View view, int position) {
                    ActivityUtils.getInstance().invokeImage(mActivity, images.get(position));
                }
            });
        }
    }

    private void updateFavouriteButton(String prodId) {
        try {
            FavouriteDBController favouriteController = new FavouriteDBController(mContext);
            favouriteController.open();
            addedToFavouriteList = favouriteController.isAlreadyFavourite(prodId);
            favouriteController.close();

            if (addedToFavouriteList) {
                addFavouriteList.setFavorite(true, false);
            } else {
                addFavouriteList.setFavorite(false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toggleFavouriteList() {
        if (!addedToFavouriteList) {
            try {
                FavouriteDBController favouriteController = new FavouriteDBController(mContext);
                favouriteController.open();
                favouriteController.insertFavouriteItem(product.id, product.name, product.imageList.get(AppConstants.INDEX_ZERO), product.sellPrice,
                        product.averageRating, product.totalSale);
                favouriteController.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                FavouriteDBController favouriteController = new FavouriteDBController(mContext);
                favouriteController.open();
                favouriteController.deleteFavouriteItemById(product.id);
                favouriteController.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        updateFavouriteButton(String.valueOf(product.id));
    }

}
