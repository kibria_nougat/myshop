package com.mcc.myshop.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.mcc.myshop.R;
import com.mcc.myshop.adapter.VerticalCategoryListAdapter;
import com.mcc.myshop.data.constant.AppConstants;
import com.mcc.myshop.listener.OnItemClickListener;
import com.mcc.myshop.model.Category;
import com.mcc.myshop.network.helper.RequestCategory;
import com.mcc.myshop.network.http.ResponseListener;
import com.mcc.myshop.utils.ActivityUtils;
import com.mcc.myshop.utils.AnalyticsUtils;

import java.util.ArrayList;

public class CategoryActivity extends BaseActivity {

    // variables
    private Context mContext;
    private Activity mActivity;
    private ArrayList<Category> categoryList;
    private VerticalCategoryListAdapter categoryListAdapter;

    // ui declaration
    private RecyclerView rvCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initListener();
        loadCategoryData();
    }

    private void initVariable() {
        mContext = getApplicationContext();
        mActivity = CategoryActivity.this;
        categoryList = new ArrayList<>();

        // analytics event trigger
        AnalyticsUtils.getAnalyticsUtils(mContext).trackEvent("Category Activity");
    }

    private void initView() {

        // set parent view
        setContentView(R.layout.activity_category);

        initToolbar();
        initLoader();
        enableBackButton();

        rvCategory = (RecyclerView) findViewById(R.id.rvCategoryList);

        // init RecyclerView
        rvCategory.setHasFixedSize(true);

        rvCategory.setLayoutManager(new LinearLayoutManager(mActivity));
        categoryListAdapter = new VerticalCategoryListAdapter(mContext, categoryList);
        rvCategory.setAdapter(categoryListAdapter);
    }

    private void initListener() {

        categoryListAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                ActivityUtils.getInstance().invokeProducts(mActivity, categoryList.get(position).name, AppConstants.TYPE_CATEGORY, categoryList.get(position).id);
            }
        });
    }

    private void loadCategoryData() {

        // Load category list
        RequestCategory requestCategory = new RequestCategory(this);
        requestCategory.setResponseListener(new ResponseListener() {
            @Override
            public void onResponse(Object data) {
                if (data != null) {
                    if (!categoryList.isEmpty()) {
                        categoryList.clear();
                    }

                    categoryList.addAll((ArrayList<Category>) data);
                    categoryListAdapter.notifyDataSetChanged();
                    hideLoader();
                }
            }
        });
        requestCategory.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
