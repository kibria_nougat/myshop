package com.mcc.myshop.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mcc.myshop.R;
import com.mcc.myshop.adapter.FavouriteListAdapter;
import com.mcc.myshop.data.sqlite.FavouriteDBController;
import com.mcc.myshop.listener.OnItemClickListener;
import com.mcc.myshop.model.FavouriteItem;
import com.mcc.myshop.utils.ActivityUtils;
import com.mcc.myshop.utils.DialogUtils;

import java.util.ArrayList;

/**
 * Created by Nasir on 7/11/17.
 */

public class FavouriteListActivity extends BaseActivity {

    // initialize variables
    private Context mContext;
    private Activity mActivity;

    private RecyclerView rvFavouriteList;
    private ArrayList<FavouriteItem> favouriteList;
    private FavouriteListAdapter favouriteListAdapter;

    private TextView info_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariables();
        initView();
        initToolbar();
        loadFavouriteData();
        initLister();
    }

    private void initVariables() {
        mContext = getApplicationContext();
        mActivity = FavouriteListActivity.this;

        favouriteList = new ArrayList<>();
    }

    private void initView() {
        setContentView(R.layout.activity_favourite_list);

        initToolbar();
        enableBackButton();
        initLoader();

        rvFavouriteList = (RecyclerView) findViewById(R.id.rvWishList);
        // init RecyclerView
        rvFavouriteList.setHasFixedSize(true);

        rvFavouriteList.setLayoutManager(new LinearLayoutManager(mActivity));
        favouriteListAdapter = new FavouriteListAdapter(mContext, favouriteList);
        rvFavouriteList.setAdapter(favouriteListAdapter);
        info_text = (TextView) findViewById(R.id.info_text);
    }

    private void initLister() {
        favouriteListAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View viewItem, int position) {

                switch (viewItem.getId()) {
                    case R.id.ivDeleteFavourite:
                        // Remove from wish
                        deleteFavouriteItemDialog(favouriteList.get(position).productId);
                        break;
                    default:

                        ActivityUtils.getInstance().invokeProductDetails(mActivity, String.valueOf(favouriteList.get(position).productId));

                        break;
                }
            }
        });
    }

    private void loadFavouriteData() {
        if (!favouriteList.isEmpty()) {
            favouriteList.clear();
        }
        try {
            FavouriteDBController favouriteController = new FavouriteDBController(mContext);
            favouriteController.open();
            favouriteList.addAll(favouriteController.getAllFavouriteData());
            favouriteController.close();

            favouriteListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (favouriteList.isEmpty()) {
            showEmptyView();
            info_text.setText(getString(R.string.empty_favourite_list));
        } else {
            hideLoader();
        }
    }

    private void deleteFavouriteItemDialog(final int productId) {

        DialogUtils.showDialogPrompt(mActivity, null, getString(R.string.delete_favourite_item), getString(R.string.dialog_btn_yes), getString(R.string.dialog_btn_no), true, new DialogUtils.DialogActionListener() {
            @Override
            public void onPositiveClick() {
                try {
                    FavouriteDBController favouriteController = new FavouriteDBController(mContext);
                    favouriteController.open();
                    favouriteController.deleteFavouriteItemById(productId);
                    favouriteController.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                loadFavouriteData();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
