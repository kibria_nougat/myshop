package com.mcc.myshop.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mcc.myshop.R;
import com.mcc.myshop.adapter.CategoryListAdapter;
import com.mcc.myshop.adapter.HomeProdListAdapter;
import com.mcc.myshop.adapter.ImageSliderAdapter;
import com.mcc.myshop.data.constant.AppConstants;
import com.mcc.myshop.data.sqlite.NotificationDBController;
import com.mcc.myshop.listener.OnItemClickListener;
import com.mcc.myshop.model.Category;
import com.mcc.myshop.model.NotificationModel;
import com.mcc.myshop.model.ProductDetail;
import com.mcc.myshop.network.helper.RequestCategory;
import com.mcc.myshop.network.helper.RequestProducts;
import com.mcc.myshop.network.http.ResponseListener;
import com.mcc.myshop.network.params.HttpParams;
import com.mcc.myshop.utils.ActivityUtils;
import com.mcc.myshop.utils.AdUtils;
import com.mcc.myshop.utils.AnalyticsUtils;
import com.mcc.myshop.utils.AppUtility;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends BaseActivity {

    // variables
    private Context mContext;
    private Activity mActivity;
    private ArrayList<Category> categoryList;
    private ArrayList<ProductDetail> featuredProdList;
    private ArrayList<ProductDetail> recentProdList;
    private ArrayList<ProductDetail> popularProdList;
    private ArrayList<ProductDetail> sampleCategoryProducts;
    private HomeProdListAdapter featuredProdListAdapter, recentProdListAdapter, popularProdListAdapter, sampleCategoryAdapter;
    private static final int COLUMN_SPAN_COUNT = 2;

    // ui declaration
    private RelativeLayout featureParent, recentParent, popularParent, sampleCatParent;
    private ProgressBar featureProgress, recentProgress, popularProgress, sampleProgress;
    private TextView tvFeaturedListTitle, tvFeaturedListAll, tvRecentListTitle, tvRecentListAll, tvPopularListTitle, tvPopularListAll,
            tvSampleCategoryTitle, tvSampleCategoryAll, tvNotificationCounter;
    private ImageView imgNotification;
    private SearchView ivSearchIcon;

    // view pager image slider
    private ViewPager mPager;
    // constants
    private static final int TIMER_DURATION = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariable();
        initView();
        initListener();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNotificationCounter();

        // load full screen ad
        AdUtils.getInstance(mContext).loadFullScreenAd(mActivity);
    }

    private void initVariable() {
        mContext = getApplicationContext();
        mActivity = MainActivity.this;
        categoryList = new ArrayList<>();
        featuredProdList = new ArrayList<>();
        recentProdList = new ArrayList<>();
        popularProdList = new ArrayList<>();
        sampleCategoryProducts = new ArrayList<>();

        // analytics event trigger
        AnalyticsUtils.getAnalyticsUtils(mContext).trackEvent("Main Activity");
    }

    private void initView() {

        // set parent view
        setContentView(R.layout.activity_main);

        // initiate drawer and toolbar
        initToolbar();
        initDrawer();
        initLoader();

        // cart counter
        imgNotification = (ImageView) findViewById(R.id.imgNotification);
        ivSearchIcon = (SearchView) findViewById(R.id.ivSearchIcon);
        tvNotificationCounter = (TextView) findViewById(R.id.tvNotificationCounter);


        // featured list ui
        RelativeLayout lytFeaturedList = (RelativeLayout) findViewById(R.id.lytFeaturedList);
        RecyclerView rvFeaturedList = (RecyclerView) lytFeaturedList.findViewById(R.id.homeRecyclerView);
        tvFeaturedListTitle = (TextView) lytFeaturedList.findViewById(R.id.tvListTitle);
        tvFeaturedListAll = (TextView) lytFeaturedList.findViewById(R.id.tvSeeAll);
        featureParent = (RelativeLayout) lytFeaturedList.findViewById(R.id.parentPanel);
        featureProgress = (ProgressBar) lytFeaturedList.findViewById(R.id.sectionProgress);
        //rvFeaturedList.addItemDecoration(new GridSpacingItemDecoration(2, (int) getResources().getDimension(R.dimen.pad_margin_2dp), false));

        // recent list ui
        RelativeLayout lytRecentList = (RelativeLayout) findViewById(R.id.lytRecentList);
        RecyclerView rvRecentList = (RecyclerView) lytRecentList.findViewById(R.id.homeRecyclerView);
        tvRecentListTitle = (TextView) lytRecentList.findViewById(R.id.tvListTitle);
        tvRecentListAll = (TextView) lytRecentList.findViewById(R.id.tvSeeAll);
        recentParent = (RelativeLayout) lytRecentList.findViewById(R.id.parentPanel);
        recentProgress = (ProgressBar) lytRecentList.findViewById(R.id.sectionProgress);

        // popular list ui
        RelativeLayout lytPopularList = (RelativeLayout) findViewById(R.id.lytPopularList);
        RecyclerView rvPopularList = (RecyclerView) lytPopularList.findViewById(R.id.homeRecyclerView);
        tvPopularListTitle = (TextView) lytPopularList.findViewById(R.id.tvListTitle);
        tvPopularListAll = (TextView) lytPopularList.findViewById(R.id.tvSeeAll);
        popularParent = (RelativeLayout) lytPopularList.findViewById(R.id.parentPanel);
        popularProgress = (ProgressBar) lytPopularList.findViewById(R.id.sectionProgress);

        // a sample category (you can choose your own category by replacing this)
        RelativeLayout lytSampleCategory = (RelativeLayout) findViewById(R.id.lytSampleCategory);
        RecyclerView rvSampleCategory = (RecyclerView) lytSampleCategory.findViewById(R.id.homeRecyclerView);
        tvSampleCategoryTitle = (TextView) lytSampleCategory.findViewById(R.id.tvListTitle);
        tvSampleCategoryAll = (TextView) lytSampleCategory.findViewById(R.id.tvSeeAll);
        sampleCatParent = (RelativeLayout) lytSampleCategory.findViewById(R.id.parentPanel);
        sampleProgress = (ProgressBar) lytSampleCategory.findViewById(R.id.sectionProgress);

        // init featured product RecyclerView
        rvFeaturedList.setLayoutManager(new GridLayoutManager(mActivity, COLUMN_SPAN_COUNT));
        featuredProdListAdapter = new HomeProdListAdapter(mContext, featuredProdList);
        rvFeaturedList.setAdapter(featuredProdListAdapter);

        // init recent product RecyclerView
        rvRecentList.setLayoutManager(new GridLayoutManager(mActivity, COLUMN_SPAN_COUNT));
        recentProdListAdapter = new HomeProdListAdapter(mContext, recentProdList);
        rvRecentList.setAdapter(recentProdListAdapter);

        // init popular product RecyclerView
        rvPopularList.setLayoutManager(new GridLayoutManager(mActivity, COLUMN_SPAN_COUNT));
        popularProdListAdapter = new HomeProdListAdapter(mContext, popularProdList);
        rvPopularList.setAdapter(popularProdListAdapter);

        // init tech product RecyclerView
        rvSampleCategory.setLayoutManager(new GridLayoutManager(mActivity, COLUMN_SPAN_COUNT));
        sampleCategoryAdapter = new HomeProdListAdapter(mContext, sampleCategoryProducts);
        rvSampleCategory.setAdapter(sampleCategoryAdapter);


        AppUtility.noInternetWarning(findViewById(R.id.parentPanel), mContext);
        if (!AppUtility.isNetworkAvailable(mContext)) {
            showEmptyView();
        }
    }

    private void loadData() {
        // Load category list
        RequestCategory requestCategory = new RequestCategory(this);
        requestCategory.setResponseListener(new ResponseListener() {
            @Override
            public void onResponse(Object data) {
                if (data != null) {
                    if (!categoryList.isEmpty()) {
                        categoryList.clear();
                    }

                    categoryList.addAll((ArrayList<Category>) data);

                    if (!categoryList.isEmpty()) {
                        // load slider images
                        loadCategorySlider();
                        hideLoader();
                    }
                }
            }
        });
        requestCategory.execute();

        // Load featured products
        RequestProducts featuredProducts = new RequestProducts(mActivity, AppConstants.INITIAL_PAGE_NUMBER, AppConstants.MAX_PER_PAGE, HttpParams.KEY_FEATURED, AppConstants.TYPE_FEATURED);
        featuredProducts.setResponseListener(new ResponseListener() {
            @Override
            public void onResponse(Object data) {
                if (data != null) {
                    featuredProdList.addAll((ArrayList<ProductDetail>) data);

                    if (!featuredProdList.isEmpty()) {
                        featureParent.setVisibility(View.VISIBLE);
                        tvFeaturedListTitle.setText((getString(R.string.featured_item) + "(" + featuredProdList.size() + ")"));   // set list title
                        featuredProdListAdapter.setDisplayCount(AppConstants.HOME_ITEM_MAX);                                    // set display item limit
                        featuredProdListAdapter.notifyDataSetChanged();
                    }
                }
                featureProgress.setVisibility(View.GONE);
                hideLoader();
            }
        });
        featuredProducts.execute();

        // Load recent products
        RequestProducts recentProducts = new RequestProducts(mActivity, AppConstants.INITIAL_PAGE_NUMBER, AppConstants.MAX_PER_PAGE, HttpParams.KEY_RECENT + AppUtility.getRecentProductDateTime(AppConstants.NUMBER_OF_DAYS_BEFORE), AppConstants.TYPE_RECENT);
        recentProducts.setResponseListener(new ResponseListener() {
            @Override
            public void onResponse(Object data) {
                if (data != null) {
                    recentProdList.addAll((ArrayList<ProductDetail>) data);

                    if (!recentProdList.isEmpty()) {
                        recentParent.setVisibility(View.VISIBLE);
                        tvRecentListTitle.setText((getString(R.string.recent_item) + "(" + recentProdList.size() + ")"));
                        recentProdListAdapter.setDisplayCount(AppConstants.HOME_ITEM_MAX);
                        recentProdListAdapter.notifyDataSetChanged();
                    }
                }
                recentProgress.setVisibility(View.GONE);
                hideLoader();
            }
        });
        recentProducts.execute();

        // Load popular product
        RequestProducts popularProducts = new RequestProducts(mContext, AppConstants.TYPE_POPULAR);
        popularProducts.setResponseListener(new ResponseListener() {
            @Override
            public void onResponse(Object data) {
                if (data != null) {
                    popularProdList.addAll((ArrayList<ProductDetail>) data);

                    if (!popularProdList.isEmpty()) {
                        popularParent.setVisibility(View.VISIBLE);
                        tvPopularListTitle.setText((getString(R.string.popular_item) + "(" + popularProdList.size() + ")"));
                        popularProdListAdapter.setDisplayCount(AppConstants.HOME_ITEM_MAX);
                        popularProdListAdapter.notifyDataSetChanged();
                    }
                }
                popularProgress.setVisibility(View.GONE);
                hideLoader();
            }
        });
        popularProducts.execute();

        // Load tech categories products as sample category
        RequestProducts requestCatProduct = new RequestProducts(mActivity, AppConstants.INITIAL_PAGE_NUMBER, AppConstants.MAX_PER_PAGE, AppConstants.CATEGORY_TEC_PRODUCTS_ID, AppConstants.TYPE_CATEGORY);
        requestCatProduct.setResponseListener(new ResponseListener() {
            @Override
            public void onResponse(Object data) {
                if (data != null) {
                    sampleCategoryProducts.addAll((ArrayList<ProductDetail>) data);

                    if (!sampleCategoryProducts.isEmpty()) {
                        sampleCatParent.setVisibility(View.VISIBLE);
                        tvSampleCategoryTitle.setText((getString(R.string.sample_category_title) + "(" + sampleCategoryProducts.size() + ")"));
                        sampleCategoryAdapter.setDisplayCount(AppConstants.HOME_ITEM_MAX);
                        sampleCategoryAdapter.notifyDataSetChanged();
                    }
                }
                sampleProgress.setVisibility(View.GONE);
                hideLoader();
            }
        });
        requestCatProduct.execute();
    }

    private void initListener() {

        featuredProdListAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                int productId = featuredProdList.get(position).id;
                if (productId > AppConstants.VALUE_ZERO) {
                    ActivityUtils.getInstance().invokeProductDetails(mActivity, String.valueOf(productId));
                }
            }
        });

        recentProdListAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                int productId = recentProdList.get(position).id;
                if (productId > AppConstants.VALUE_ZERO) {
                    ActivityUtils.getInstance().invokeProductDetails(mActivity, String.valueOf(productId));
                }
            }
        });

        popularProdListAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                int productId = popularProdList.get(position).id;
                if (productId > AppConstants.VALUE_ZERO) {
                    ActivityUtils.getInstance().invokeProductDetails(mActivity, String.valueOf(productId));
                }
            }
        });
        sampleCategoryAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                int productId = sampleCategoryProducts.get(position).id;
                if (productId > AppConstants.VALUE_ZERO) {
                    ActivityUtils.getInstance().invokeProductDetails(mActivity, String.valueOf(productId));
                }
            }
        });
        // toolbar notification action listener
        imgNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAdThenActivity(NotificationActivity.class);

                /**
                 * if you don't want to show notification then disable
                 * disable previous line and use line given bellow
                 */
                //ActivityUtils.getInstance().invokeActivity(mActivity, NotificationActivity.class, false);

            }
        });

        // search view on query submit
        ivSearchIcon.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ActivityUtils.getInstance().invokeSearchActivity(mActivity, query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // See all listener
        tvFeaturedListAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.getInstance().invokeProducts(mActivity, getString(R.string.featured_items), AppConstants.TYPE_FEATURED, AppConstants.NO_CATEGORY);
            }
        });

        tvRecentListAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.getInstance().invokeProducts(mActivity, getString(R.string.recent_items), AppConstants.TYPE_RECENT, AppConstants.NO_CATEGORY);
            }
        });

        tvPopularListAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.getInstance().invokeProducts(mActivity, getString(R.string.popular_items), AppConstants.TYPE_POPULAR, AppConstants.NO_CATEGORY);
            }
        });

        tvSampleCategoryAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.getInstance().invokeProducts(mActivity, getString(R.string.tech_discovery), AppConstants.TYPE_CATEGORY, AppConstants.CATEGORY_TEC_PRODUCTS_ID);
            }
        });
    }

    private void loadCategorySlider() {
        final ArrayList<String> imageList = new ArrayList<>();
        for (int i = 0; i < categoryList.size(); i++) {
            imageList.add(categoryList.get(i).image);
        }
        ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(mContext, imageList);
        mPager = (ViewPager) findViewById(R.id.vpImageSlider);
        mPager.setAdapter(imageSliderAdapter);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.sliderIndicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                int setPosition = mPager.getCurrentItem() + 1;
                if (setPosition == imageList.size()) {
                    setPosition = AppConstants.VALUE_ZERO;
                }
                mPager.setCurrentItem(setPosition, true);
            }
        };

        //  Auto animated timer
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, TIMER_DURATION, TIMER_DURATION);


        // view page on item click listener
        imageSliderAdapter.setItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemListener(View view, int position) {
                ActivityUtils.getInstance().invokeProducts(mActivity, categoryList.get(position).name, AppConstants.TYPE_CATEGORY, categoryList.get(position).id);
            }
        });

    }

    private void loadNotificationCounter() {
        try {
            NotificationDBController notifyController = new NotificationDBController(mContext);
            notifyController.open();
            ArrayList<NotificationModel> notifyList = notifyController.getUnreadNotification();
            notifyController.close();

            if (notifyList.isEmpty()) {
                tvNotificationCounter.setVisibility(View.GONE);
            } else {
                tvNotificationCounter.setVisibility(View.VISIBLE);
                tvNotificationCounter.setText(String.valueOf(notifyList.size()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            tvNotificationCounter.setVisibility(View.GONE);
        }
    }

}
