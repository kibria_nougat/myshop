package com.mcc.myshop.data.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.mcc.myshop.data.constant.AppConstants;
import com.mcc.myshop.model.FavouriteItem;
import java.util.ArrayList;

public class FavouriteDBController {

    private DatabaseHelper dbHelper;
    private Context mContext;
    private SQLiteDatabase database;

    public FavouriteDBController(Context context) {
        mContext = context;
    }

    public FavouriteDBController open() throws SQLException {
        dbHelper = new DatabaseHelper(mContext);
        database = dbHelper.getWritableDatabase();
        return this;
    }
    public void close() {
        dbHelper.close();
    }

    public long insertFavouriteItem(int productId, String name, String images, float price, float ratting, int orderCount) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelper.KEY_PRODUCT_ID, productId);
        contentValue.put(DatabaseHelper.KEY_NAME, name);
        contentValue.put(DatabaseHelper.KEY_IMAGES, images);
        contentValue.put(DatabaseHelper.KEY_PRICE, price);
        contentValue.put(DatabaseHelper.KEY_RATTING, ratting);
        contentValue.put(DatabaseHelper.KEY_ORDER_COUNT, orderCount);

        return database.insert(DatabaseHelper.TABLE_FAVOURITE, null, contentValue);
    }

    public ArrayList<FavouriteItem> getAllFavouriteData()
    {
        ArrayList<FavouriteItem> favouriteList = new ArrayList<>();

        Cursor cursor = database.rawQuery("select * from "+DatabaseHelper.TABLE_FAVOURITE, null);
        if (cursor != null && cursor.getCount() > 0) {
            try {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    int productId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_PRODUCT_ID));
                    String name   = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_NAME));
                    String images = cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_IMAGES));
                    float price   = cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.KEY_PRICE));
                    float ratting = cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.KEY_RATTING));
                    int orderCount= cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_ORDER_COUNT));

                    if (productId > AppConstants.VALUE_ZERO) {
                        favouriteList.add(new FavouriteItem(productId, name, images, price, ratting, orderCount));
                    }
                    cursor.moveToNext();
                }
            } catch (Exception ex) {
            }
        }
        return favouriteList;
    }

    public int updateFavouriteItem(long id, int productId, String name, String images, float price, float ratting, int orderCount) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.KEY_PRODUCT_ID, productId);
        contentValues.put(DatabaseHelper.KEY_NAME, name);
        contentValues.put(DatabaseHelper.KEY_IMAGES, images);
        contentValues.put(DatabaseHelper.KEY_PRICE, price);
        contentValues.put(DatabaseHelper.KEY_RATTING, ratting);
        contentValues.put(DatabaseHelper.KEY_ORDER_COUNT, orderCount);

        int updateStatus = database.update(DatabaseHelper.TABLE_FAVOURITE, contentValues,
                DatabaseHelper.KEY_ID + " = " + id, null);
        return updateStatus;
    }

    public void deleteFavouriteItemById(long productId) {
        database.delete(DatabaseHelper.TABLE_FAVOURITE, DatabaseHelper.KEY_PRODUCT_ID + "=" + productId, null);
    }
    public boolean isAlreadyFavourite(String productId) {
        Cursor cursor = database.rawQuery("select "+DatabaseHelper.KEY_PRODUCT_ID+" from " + DatabaseHelper.TABLE_FAVOURITE + " where " + DatabaseHelper.KEY_PRODUCT_ID + "=" + productId + "", null);
        if(cursor!=null && cursor.getCount()>0){
            cursor.close();
            return true;
        }
        cursor.close();
        return false;
    }
    public void deleteAllFavouriteData() {
        database.delete(DatabaseHelper.TABLE_FAVOURITE, null, null);
    }

    public int countFavouriteProduct(){
        int numOfRows = (int) DatabaseUtils.queryNumEntries(database, DatabaseHelper.TABLE_FAVOURITE);
        return numOfRows;
    }

    private void dropFavouriteTable() {
        String sql = "drop table " + DatabaseHelper.TABLE_FAVOURITE;
        try {
            database.execSQL(sql);
        } catch (SQLException e) {

        }
    }
//    public class CustomComparator implements Comparator<ProductResponseModel> {
//        @Override
//        public int compare(ProductResponseModel p1, ProductResponseModel p2) {
//            Long t1 = p1.postTimeStamp;
//            Long t2 = p2.postTimeStamp;
//            return t1.compareTo(t2);
//        }
//    }
}
